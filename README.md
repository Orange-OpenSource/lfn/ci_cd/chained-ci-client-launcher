# Chained CI client Launcher

## Purpose

Gives a docker for triggering and monitoring a chained ci.
It's done to trigger "gates" on OpenStack and has such has mandatory variables
that needs to be sets

## Environment variables

### MANDATORY

<!-- markdownlint-disable line-length -->
| Name                      | Other Name           | Purpose                                               |
|---------------------------|----------------------|-------------------------------------------------------|
| `NAME`                    | N/A                  | base part for the name of tenant and user             |
| `DOCKER_TAG_NAME`         | N/A                  | name of docker tag environment variable in chained ci |
| `SCENARIO`                | N/A                  | chained ci scenario name                              |
| `CHAINED_CI_PROJECT_ID`   | N/A                  | chained ci project id                                 |
| `CHAINED_CI_LAUNCH_TOKEN` | N/A                  | trigger token value                                   |
| `DOCKER_TAG`              | `CI_COMMIT_REF_SLUG` | value for docker tag                                  |
| `ID`                      | `CI_PIPELINE_IID`    | unique identifier to use                              |
<!-- markdownlint-enable line-length -->

### Optional

<!-- markdownlint-disable line-length -->
| Name                       | Purpose                                          |
|----------------------------|--------------------------------------------------|
| `WAIT_FOR_END`             | wait for end of pipeline (default: `True`)       |
| `GITLAB_MAIN_URL`          | gitlab base url (default: <https://gitlab.com>)  |
| `CHAINED_CI_PRIVATE_TOKEN` | private token. ⚠️ Mandatory when waiting for end |
| `CHAINED_CI_BRANCH`        | branch for chained ci (default: `master`)        |
| `EXTRA_VAR_1_NAME`         | Name of extra variable 1                         |
| `EXTRA_VAR_1_VALUE`        | value of extra variable 1                        |
| `EXTRA_VAR_2_NAME`         | Name of extra variable 2                         |
| `EXTRA_VAR_2_VALUE`        | value of extra variable 2                        |
| `EXTRA_VAR_3_NAME`         | Name of extra variable 3                         |
| `EXTRA_VAR_3_VALUE`        | value of extra variable 3                        |
<!-- markdownlint-enable line-length -->

### Usage

Add into CI/CD variables of your project the sensitive values
(`CHAINED_CI_LAUNCH_TOKEN` and `CHAINED_CI_PRIVATE_TOKEN`).

Create a step in your CI with required values:

```yaml
variables:
  CHAINED_CI_PROJECT_ID: 1234

deploy_a_gate:
  stage: test
  variables:
    NAME: my-gate
    SCENARIO: gating_scenario
    DOCKER_TAG_NAME: MY_GATE_DOCKER_TAG
  image:
    registry.gitlab.com/orange-opensource/lfn/ci_cd/chained-ci-client-launcher
  script:
    - /usr/src/app/chained-ci-client-launcher/chained-ci-client.py

clean_gate:
  stage: clean
  variables:
    NAME: my-gate
    SCENARIO: gating_scenario_clean
    DOCKER_TAG_NAME: MY_GATE_DOCKER_TAG
    WAIT_FOR_END: False
  image:
    registry.gitlab.com/orange-opensource/lfn/ci_cd/chained-ci-client-launcher
  script:
    - /usr/src/app/chained-ci-client-launcher/chained-ci-client.py
```

or in one step:

```yaml
variables:
  CHAINED_CI_PROJECT_ID: 1234

deploy_a_gate:
  stage: test
  variables:
    NAME: my-gate
    SCENARIO: gating_scenario
    DOCKER_TAG_NAME: MY_GATE_DOCKER_TAG
  image:
    registry.gitlab.com/orange-opensource/lfn/ci_cd/chained-ci-client-launcher
  script:
    - /usr/src/app/chained-ci-client-launcher/chained-ci-client.py
  after_script:
    - export WAIT_FOR_END=False
    - export SCENARIO=gating_scenario_clean
    - /usr/src/app/chained-ci-client-launcher/chained-ci-client.py
```
