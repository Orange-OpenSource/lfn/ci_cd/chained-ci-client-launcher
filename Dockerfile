FROM python:3.13-alpine@sha256:323a717dc4a010fee21e3f1aac738ee10bb485de4e7593ce242b36ee48d6b352
LABEL maintainer="David Blaisonneau <david.blaisonneau@orange.com>"

ARG VCS_REF
ARG BUILD_DATE
ARG USER=user
ARG SRC=/usr/src/app/chained-ci-client-launcher


# renovate: datasource=repology depName=alpine_3_21/libcrypto3 versioning=loose
ENV LIBCRYPTO3_VERSION=3.3.3-r0
# renovate: datasource=repology depName=alpine_3_21/libssl3 versioning=loose
ENV LIBSSL3_VERSION=3.3.3-r0
# renovate: datasource=repology depName=alpine_3_21/busybox versioning=loose
ENV BUSYBOX_VERSION=1.37.0-r12
# renovate: datasource=repology depName=alpine_3_21/busybox-binsh versioning=loose
ENV BUSYBOX_BINSH_VERSION=1.37.0-r12
# renovate: datasource=repology depName=alpine_3_21/ssl_client versioning=loose
ENV SSL_CLIENT_VERSION=1.37.0-r12

ENV HOME /home/$USER

COPY requirements.txt $SRC/
WORKDIR $SRC

RUN apk add --no-cache \
            busybox="${BUSYBOX_VERSION}" \
            busybox-binsh="${BUSYBOX_BINSH_VERSION}" \
            libcrypto3="${LIBCRYPTO3_VERSION}" \
            libssl3="${LIBSSL3_VERSION}" \
            ssl_client="${SSL_CLIENT_VERSION}" \
            &&\
      adduser -D $USER &&\
      pip install --no-cache-dir -r requirements.txt && \
      rm -rf /var/lib/apt/lists/* /tmp/* &&\
      rm -rf ~/.cache/pip

COPY chained-ci-client.py $SRC/

USER $USER
WORKDIR $HOME
CMD ["./chained-ci-client.py"]
