#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

import os
import sys
import time
import gitlab

print("*** Launching a Chained CI gate")

gitlab_main_url = os.getenv('GITLAB_MAIN_URL', 'https://gitlab.com')

launch_token = os.getenv('CHAINED_CI_LAUNCH_TOKEN')
if not launch_token:
  print("/!\ Environment variable 'CHAINED_CI_LAUNCH_TOKEN' must be given")
  print("      It's used to trigger the pipeline, exiting.")
  sys.exit(os.EX_USAGE)

client = gitlab.Gitlab(gitlab_main_url)
private_token = os.getenv('CHAINED_CI_PRIVATE_TOKEN')

wait_for_end_test = os.getenv('WAIT_FOR_END', 'True')
wait_for_end = False
if wait_for_end_test == "True" or wait_for_end_test == "true":
  wait_for_end = True
  if not private_token:
    print(
      "/!\ Environment variable 'CHAINED_CI_PRIVATE_TOKEN' {}".format(
        "must be given when 'WAIT_FOR_END' is true"))
    print("      It's used to check the pipeline status, exiting.")
    sys.exit(os.EX_USAGE)

if private_token:
  client = gitlab.Gitlab(gitlab_main_url, private_token=private_token)

bad_vars = False

name = os.getenv('NAME')
if not name:
  print("/!\ Environment variable 'NAME' must be given")
  print("      It's used to set the name and tenant for gating.")
  bad_vars = True

docker_tag_name = os.getenv('DOCKER_TAG_NAME')
if not docker_tag_name:
  print("/!\ Environment variable 'DOCKER_TAG_NAME' must be given")
  print("      It's used to set the name of the docker tag used.")
  bad_vars = True

docker_tag = os.getenv('DOCKER_TAG', os.getenv('CI_COMMIT_REF_SLUG'))
if not docker_tag:
  print("/!\ Environment variable 'DOCKER_TAG' must be given")
  print("      It's used to select the right docker tag for the gating.")
  bad_vars = True

pod = os.getenv('SCENARIO')
if not pod:
  print("/!\ Environment variable 'SCENARIO' must be given")
  print("      It's used to set the name chained ci deployment to use.")
  bad_vars = True

project_id = os.getenv('CHAINED_CI_PROJECT_ID')
if not project_id:
  print("/!\ Environment variable 'CHAINED_CI_PROJECT_ID' must be given")
  print("      It's used to trigger the pipeline and check the status.")
  bad_vars = True

identifier = os.getenv('ID', os.getenv('CI_PIPELINE_IID'))
if not identifier:
  print("/!\ Environment variable 'ID' must be given")
  print("      It's used to generate a specific project for the gating.")
  bad_vars = True

if bad_vars:
  print(
    "/!\ At least one mandatory environment variable is not given, exiting.",
  )
  sys.exit(os.EX_USAGE)

chained_ci_branch = os.getenv('CHAINED_CI_BRANCH', 'master')
scenario_launch_name = os.getenv('SCENARIO_LAUNCH_NAME', 'POD')
extra_var_1_name = os.getenv('EXTRA_VAR_1_NAME')
extra_var_1_value = os.getenv('EXTRA_VAR_1_VALUE')
extra_var_2_name = os.getenv('EXTRA_VAR_2_NAME')
extra_var_2_value = os.getenv('EXTRA_VAR_2_VALUE')
extra_var_3_name = os.getenv('EXTRA_VAR_3_NAME')
extra_var_3_value = os.getenv('EXTRA_VAR_3_VALUE')
extra_var_4_name = os.getenv('EXTRA_VAR_4_NAME')
extra_var_4_value = os.getenv('EXTRA_VAR_4_VALUE')
extra_var_5_name = os.getenv('EXTRA_VAR_5_NAME')
extra_var_5_value = os.getenv('EXTRA_VAR_5_VALUE')
extra_var_6_name = os.getenv('EXTRA_VAR_6_NAME')
extra_var_6_value = os.getenv('EXTRA_VAR_6_VALUE')

project = client.projects.get(project_id)
if not project:
  print("/!\ issue will retrieving chained ci project, exiting")
  sys.exit(os.EX_UNAVAILABLE)

web_url= project.web_url

pipeline_variables = {
  'TENANT_NAME': "{}-{}".format(name, identifier),
  'USER_NAME': "{}-{}-ci".format(name, identifier),
  docker_tag_name: docker_tag,
  scenario_launch_name: pod,
  'ENVIRONMENT_ID': identifier,
}

if extra_var_1_name:
  print("*** adding '{}' variable with value '{}' to the list".format(
    extra_var_1_name,
    extra_var_1_value,
    ))
  pipeline_variables[extra_var_1_name] = extra_var_1_value

if extra_var_2_name:
  print("*** adding '{}' variable with value '{}' to the list".format(
    extra_var_2_name,
    extra_var_2_value,
    ))
  pipeline_variables[extra_var_2_name] = extra_var_2_value

if extra_var_3_name:
  print("*** adding '{}' variable with value '{}' to the list".format(
    extra_var_3_name,
    extra_var_3_value,
    ))
  pipeline_variables[extra_var_3_name] = extra_var_3_value

if extra_var_4_name:
  print("*** adding '{}' variable with value '{}' to the list".format(
    extra_var_4_name,
    extra_var_4_value,
    ))
  pipeline_variables[extra_var_4_name] = extra_var_4_value

if extra_var_5_name:
  print("*** adding '{}' variable with value '{}' to the list".format(
    extra_var_5_name,
    extra_var_5_value,
    ))
  pipeline_variables[extra_var_5_name] = extra_var_5_value

if extra_var_6_name:
  print("*** adding '{}' variable with value '{}' to the list".format(
    extra_var_6_name,
    extra_var_6_value,
    ))
  pipeline_variables[extra_var_6_name] = extra_var_6_value

print("*** will use th following branch: {}".format(chained_ci_branch))
print("*** will use the following variables: {}".format(pipeline_variables))

pipeline = project.trigger_pipeline(
  chained_ci_branch,
  launch_token,
  pipeline_variables,
  )
if not pipeline:
  print("/!\ Issue will creating pipeline, exiting.")
  sys.exit(os.EX_UNAVAILABLE)

if not pipeline.id:
  print("/!\ Pipeline has no id, exiting")
  sys.exit(os.EX_IOERR)

pipeline_id = pipeline.id
pipeline_url = "{}/-/pipelines/{}".format(web_url, pipeline_id)

print("*** Pipeline has been launched: {}".format(pipeline_url))
i = 0
if wait_for_end:
  print('*** Waiting for the end of the pipeline')
  while True:
    if i != 9:
      sys.stdout.write('.')
      sys.stdout.flush()
      i += 1
    else:
      sys.stdout.write('#')
      sys.stdout.flush()
      i = 0
    time.sleep(10)
    pipeline = project.pipelines.get(pipeline_id, retry_transient_errors=True)
    if pipeline.status == "success" :
      print("\n*** Inner pipeline is successful")
      sys.exit(os.EX_OK)
    elif pipeline.status == "failed" :
      print("\n/!\ Inner pipeline has failed")
      print("     --> look at {}".format(pipeline_url))
      sys.exit(os.EX_SOFTWARE)
    elif pipeline.status == "canceled" :
      print("\n/!\ Inner pipeline has been canceled")
      print("     --> look at {}".format(pipeline_url))
      sys.exit(os.EX_UNAVAILABLE)
else:
  print("*** not waiting")
  sys.exit(os.EX_OK)
